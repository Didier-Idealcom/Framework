<!--begin::Dropdown-->
<div class="btn-group">
    <button type="button" href="#save_close" class="btn btn-sm btn-primary my-link__save">Save Changes</button>
    <button type="button" class="btn btn-sm btn-primary dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"></button>
    <div class="dropdown-menu menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-7 w-175px py-4">
        <div class="menu-item px-3">
            <a href="#save_stay" class="menu-link px-3 my-link__save">
                <span class="menu-icon">
                    <i class="ki-duotone ki-update-file fs-1">
                        <span class="path1"></span>
                        <span class="path2"></span>
                        <span class="path3"></span>
                        <span class="path4"></span>
                    </i>
                </span>
                <span class="menu-text">Save &amp; Stay</span>
            </a>
        </div>
        <div class="menu-item px-3">
            <a href="#save_new" class="menu-link px-3 my-link__save">
                <span class="menu-icon">
                    <i class="ki-duotone ki-add-files fs-1">
                        <span class="path1"></span>
                        <span class="path2"></span>
                        <span class="path3"></span>
                        <span class="path4"></span>
                    </i>
                </span>
                <span class="menu-text">Save &amp; New</span>
            </a>
        </div>
        <div class="menu-item px-3">
            <a href="#save_close" class="menu-link px-3 my-link__save">
                <span class="menu-icon">
                    <i class="ki-duotone ki-file-deleted fs-1">
                        <span class="path1"></span>
                        <span class="path2"></span>
                        <span class="path3"></span>
                        <span class="path4"></span>
                    </i>
                </span>
                <span class="menu-text">Save &amp; Close</span>
            </a>
        </div>
    </div>
</div>
<!--end::Dropdown-->
